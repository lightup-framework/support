<?php

namespace Lightup\Support;

use Exception;
use Lightup\Framework\Container as ContainerContract;
use Lightup\Support\Registry\AliasesRegistry;
use Lightup\Support\Exception\Container\LoopException;
use Lightup\Support\Exception\Container\MethodDoesntExistException;
use Lightup\Support\Registry\Registry;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use ReflectionClass;
use ReflectionNamedType;

final class Container implements ContainerContract
{
    private AliasesRegistry $aliasesRegistry;
    private Registry $makingRegistry;
    private Registry $objectRegistry;
    private static Container $_instance;

    public function __construct()
    {
        $this->makingRegistry = new Registry();
        $this->objectRegistry = new Registry();

        $this->aliasesRegistry = new AliasesRegistry();
        $this->objectRegistry->add(AliasesRegistry::class, $this->aliasesRegistry);
        $this->objectRegistry->add(ContainerContract::class, $this);
    }

    public static function getInstance() : self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
            self::$_instance->objectRegistry->add(self::class, self::$_instance);
        }

        return self::$_instance;
    }

    public function get(string $id)
    {
        $concrete = $this->aliasesRegistry->get($id);

        if (!$this->objectRegistry->has($concrete)) {
            $this->objectRegistry->add($concrete, $this->makeObject($concrete));
        }

        return $this->objectRegistry->get($concrete);
    }

    public function executeObjectMethod(string $class, string $method, array $params = []): mixed
    {
        $concrete = $this->aliasesRegistry->get($class);
        $instance = $this->get($concrete);

        $reflection = new ReflectionClass($concrete);

        if (!$reflection->hasMethod($method)) {
            throw new MethodDoesntExistException($concrete, $method);
        }

        $methodReflection = $reflection->getMethod($method);
        $parameters = $methodReflection->getParameters();
        $objectsParams = $this->getParamsAsObjects($parameters);

        return $instance->{$method}(...array_merge($objectsParams, $params));
    }

    private function makeObject($class): object
    {
        if ($this->makingRegistry->has($class)) {
            throw new LoopException($class);
        }

        $this->makingRegistry->add($class, '');

        $reflection = new ReflectionClass($class);
        $constructor = $reflection->getConstructor();

        if ($constructor) {
            $parameters = $constructor->getParameters();
            $constructorParams = $this->getParamsAsObjects($parameters);

            $instance = new $class(...$constructorParams);
        } else {
            $instance = new $class();
        }

        $this->makingRegistry->remove($class);

        return $instance;
    }

    public function bind(string $class, object|string $mix): void
    {
        if (gettype($mix) === 'string') {
            $this->aliasesRegistry->add($class, $mix);
        } else {
            $this->objectRegistry->add($class, $mix);
        }
    }


    public function has(string $id): bool
    {
        return $this->aliasesRegistry->has($id);
    }


    private function getParamsAsObjects(array $parameters): array
    {
        $objects = [];

        foreach ($parameters as $parameter) {
            if ($parameter->hasType() && !$this->isPrimitive($parameter->getType()) && $parameter->getType()->getName() !== 'string' && $parameter->getType()->getName() !== 'mixed') {
                $objects[] = $this->get($parameter->getType()->getName());
            }
        }

        return $objects;
    }

    private function isPrimitive($type): bool
    {
        $primitives = ['int', 'float', 'bool', 'string'];

        return get_class($type) != ReflectionNamedType::class && in_array($type, $primitives);
    }
}