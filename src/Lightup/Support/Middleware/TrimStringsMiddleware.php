<?php

namespace Lightup\Support\Middleware;

use Lightup\Framework\Http\Request;
use Lightup\Framework\Middleware;

class TrimStringsMiddleware extends Middleware
{
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $inputs = $this->request->getInputs();

        foreach ($inputs as $key => $value) {
            $value = trim($value);

            $this->request->setInput($key, $value);
        }
    }
}