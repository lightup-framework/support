<?php

namespace Lightup\Support\Kernel;

interface Kernel
{
    public function handle(): void;
}