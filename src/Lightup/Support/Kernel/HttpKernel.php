<?php

namespace Lightup\Support\Kernel;

use Lightup\Framework\Configuration;
use Lightup\Framework\Container;
use Lightup\Framework\Http\Response as ResponseType;
use Lightup\Framework\Router;
use Lightup\Support\Http\Controller\ErrorController;
use Lightup\Support\Support\Session;
use Lightup\Support\TemplateEngine;
use RuntimeException;

final class HttpKernel implements Kernel
{
    private Router $router;
    private TemplateEngine $templateEngine;
    private ResponseType $response;
    private Container $container;
    private Configuration $configuration;

    private string $viewBasePath;

    public function __construct(
        Router          $router,
        TemplateEngine  $templateEngine,
        Container       $container,
        Configuration   $configuration,
    ) {
        $this->router = $router;
        $this->templateEngine = $templateEngine;
        $this->container = $container;
        $this->viewBasePath = base_path(['views']);
        $this->configuration = $configuration;
    }

    public function handle(): void
    {
        $this->aliases();
        $this->binding();

        $this->init();

        $this->bootProviders();

        if ($this->runMiddlewares()) {
            $this->resolve();
        }

        $this->dispatch();
        $this->shutdownProviders();
    }

    public function abort(ResponseType $response): void
    {
        $this->response = $response;

        $this->dispatch();
        exit;
    }

    private function aliases(): void
    {
        foreach ($this->configuration->get('aliases.class_aliases') as $key => $value) {
            class_alias($value, $key);
        }

        foreach ($this->configuration->get('aliases.short_aliases') as $key => $value) {
            $this->container->bind($key, $value);
        }
    }

    private function binding(): void
    {
        foreach ($this->configuration->get('binding') as $key => $value) {
            $this->container->bind($key, $value);
        }
    }

    private function init(): void
    {
        Session::start();
    }

    private function resolve(): void
    {
        [
            'controller' => $controller,
            'method' => $method,
            'parameters' => $parameters,
            'middlewares' => $middlewares,
        ] = $this->router->route();


        foreach ($middlewares as $middleware) {
            $response = $this->container->executeObjectMethod($middleware, 'handle');
            if ($response != null) {
                $this->response = $response;
                return;
            }
        }

        $response = $this->container->executeObjectMethod($controller, $method, $parameters);

        if ($controller != ErrorController::class) {
            $this->viewBasePath = base_path(['views']);
        }

        $this->response = $response;
    }

    private function dispatch(): void
    {
        if ($this->response == null) {
            throw new RuntimeException("Method resolve wasn't called");
        }

        http_response_code($this->response->getStatusCode());

        foreach ($this->response->getHeaders() as $header => $value) {
            if ($header == 'Location' && !empty($this->response->getParameters())) {
                Session::set('redirect_parameters', $this->response->getParameters());
                Session::writeClose();
            }

            header("{$header}: {$value}");
        }

        $parameters = $this->response->getParameters();
        if (Session::check('redirect_parameters')) {
            $parameters = array_merge($parameters, Session::pull('redirect_parameters'));
        }

        $this->templateEngine->view($this->response->getName(), $this->viewBasePath . DIRECTORY_SEPARATOR, $parameters);
    }

    private function bootProviders(): void
    {
        $providers = $this->configuration->get('providers');

        foreach ($providers as $provider) {
            $this->container->executeObjectMethod($provider, 'boot');
        }
    }

    private function shutdownProviders(): void
    {
        $providers = $this->configuration->get('providers');

        foreach ($providers as $provider) {
            $this->container->executeObjectMethod($provider, 'shutdown');
        }
    }

    private function runMiddlewares(): bool
    {
        $middlewares = $this->configuration->get('middlewares');

        foreach ($middlewares as $middleware) {
            $result = $this->container->executeObjectMethod($middleware, 'handle');

            if ($result != null) {
                $this->response = $result;
                return false;
            }
        }

        return true;
    }
}