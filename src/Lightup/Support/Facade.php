<?php

namespace Lightup\Support;

use RuntimeException;

abstract class Facade
{
    public static function getFacadeAccessor(): string
    {
        throw new RuntimeException("Facade accessor must be set");
    }

    public static function __callStatic($name, $arguments)
    {
        $container = Container::getInstance();
        return $container->executeObjectMethod(static::getFacadeAccessor(), $name, $arguments);
    }
}