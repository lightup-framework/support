<?php

namespace Lightup\Support\Exception;

use \RuntimeException;

class RegistryKeyDoesntExists extends RuntimeException
{
    public function __construct($key)
    {
        parent::__construct("Key {$key} already doesnt exists in registry");
    }
}