<?php

namespace Lightup\Support\Response;

use Lightup\Framework\Http\Response;

class View extends Response
{
    private string $name;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): View
    {
        $this->name = $name;

        return $this;
    }


}