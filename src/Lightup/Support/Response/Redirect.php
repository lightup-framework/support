<?php

namespace Lightup\Support\Response;

use Lightup\Framework\Http\Response;

class Redirect extends Response
{
    protected string $redirect;

    public function getRedirect(): string
    {
        return $this->redirect;
    }

    public function setRedirect(string $redirect): Redirect
    {
        $this->redirect = $redirect;
        $this->headers['Location'] = $redirect;

        return $this;
    }
}