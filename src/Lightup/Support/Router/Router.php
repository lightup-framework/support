<?php

namespace Lightup\Support\Router;

use Lightup\Framework\Router as RouterContract;
use Lightup\Support\Http\Controller\ErrorController;
use Lightup\Support\Http\Request;
use Lightup\Support\Route\Route;

class Router implements RouterContract
{
    private Request $request;
    private Route $route;
    private array $parameters = [];

    public function __construct(Request $request, Route $route)
    {
        $this->request = $request;
        $this->route = $route;
    }

    public function route() : array
    {
        $currentMethod = $this->request->getMethod();
        $currentPath = $this->request->getPath();

        foreach ($this->route->getRoutes()[$currentMethod] as $route) {
            $pattern = preg_replace('/\/{(.*?)}/', '/(.*?)', $route['route']);

            if (preg_match('#^' . $pattern . '$#', $currentPath, $matches)) {
                $this->parameters = array_slice($matches, 1);

                return $this->routeToArray($route);
            }
        }

        return [
            'controller' => ErrorController::class,
            'method' => 'error',
            'parameters' => [404],
            'middlewares' => [],
        ];
    }

    private function routeToArray(array $route): array
    {
        $exploded = explode('@', $route['what']);

        return [
            'controller' => $exploded[0],
            'method' => $exploded[1],
            'parameters' => $this->parameters,
            'middlewares' => $route['middlewares'],
        ];
    }
}