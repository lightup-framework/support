<?php

namespace Lightup\Support\Auth;

use Lightup\Support\Support\Cookie;
use Lightup\Support\Support\Hash;

class Auth
{
    private AuthorizationRepositoryInterface $authorizationRepository;
    private string $model;
    private string $redirect;

    public function __construct(AuthorizationRepositoryInterface $authorizationRepository)
    {
        $this->authorizationRepository = $authorizationRepository;
    }

    /**
     * Tries to login user based on $username and $password
     * using method getByUsername and setToken from AuthorizationRepositoryInterface
     */
    public function attempt($username, $password): bool
    {
        $user = $this->authorizationRepository->getByLogin($username);

        if (!$user) {
            return false;
        }

        if (Hash::verify($password, $user->password)) {
            $token = $this->str_rand(32);
            $this->authorizationRepository->setToken($username, $token);
            Cookie::set('token', $token);

            return true;
        }

        return false;
    }

    public function logout()
    {
        if ($this->authenticated()) {
            $token = Cookie::get('token');

            $user = $this->authorizationRepository->getByToken($token);
            $this->authorizationRepository->setToken($user->user, '');

            Cookie::unset('token');
        }
    }

    /**
     * Check if user is logged based on token passed in cookie
     */
    public function authenticated(): bool
    {
        if (!Cookie::check('token')) {
            return false;
        }

        $user = $this->authorizationRepository->getByToken(Cookie::get('token'));

        if ($user) {
            return true;
        }

        return false;
    }

    /**
     * Returns authenticated user
     */
    public function getAuthenticated(): object
    {
        if ($this->authenticated()) {
            $token = Cookie::get('token');

            return $this->authorizationRepository->getByToken($token);
        }
    }

    /**
     * Set model class using to authenticate
     */
    public function model(string $model)
    {
        $this->model = $model;
    }

    /**
     * Return model class
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * Set url to redirect if user is not authenticated
     */
    public function redirect(string $redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * Return redirect
     */
    public function getRedirect(): string
    {
        return $this->redirect;
    }

    private function str_rand(int $length = 64): string
    {
        $length = ($length < 4) ? 4 : $length;
        return bin2hex(random_bytes(($length-($length%2))/2));
    }
}