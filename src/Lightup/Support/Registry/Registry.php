<?php

namespace Lightup\Support\Registry;

use Lightup\Support\Exception\RegistryKeyDoesntExists;
use Lightup\Support\Exception\RegistryKeyExists;
use Lightup\Framework\Registry as RegistryContract;

class Registry implements RegistryContract
{
    protected $registry = [];

    public function has($key)
    {
        return array_key_exists($key, $this->registry);
    }

    public function add($key, $value)
    {
        if (array_key_exists($key, $this->registry)) {
            throw new RegistryKeyExists($key);
        }

        $this->registry[$key] = $value;
    }

    public function get($key)
    {
        if (!$this->has($key)) {
            throw new RegistryKeyDoesntExists($key);
        }

        return $this->registry[$key];
    }

    public function remove($key)
    {
        unset($key);
    }
}