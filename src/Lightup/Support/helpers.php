<?php

if (!function_exists('base_path')) {
    function base_path(string|array $path): string
    {
        $base = $_SERVER['APPLICATION_PATH'];

        if (is_array($path)) {
            foreach ($path as $dir) {
                $base .= DIRECTORY_SEPARATOR . $dir;
            }
        } else {
            $base .= DIRECTORY_SEPARATOR . $path;
        }

        return $base;
    }
}

if (!function_exists('env')) {
    function env(string $property, mixed $default = null): string
    {
        $prop = getenv($property);

        if ($prop === false) {
            $prop = $default;
        }

        return $prop;
    }
}
