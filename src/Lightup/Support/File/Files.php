<?php

namespace Lightup\Support\File;

use Closure;

final class Files
{
    public static function createFile(string $path): void
    {
        $path = Files::sanitizePath($path);
        if (!Files::exists($path)) {
            touch($path);
        }
    }

    public static function createDirectory(string $path): void
    {
        $path = Files::sanitizePath($path);
        if (!Files::exists($path)) {
            mkdir($path);
        }
    }

    public static function exists(string $path): bool
    {
        return file_exists(Files::sanitizePath($path));
    }

    public static function removeFile(string|array $path): void
    {
        if (gettype($path) == "array") {
            foreach ($path as $file) {
                Files::removeFile($file);
            }
        } else {
            unlink($path);
        }
    }

    public static function removeDirectory(string $path, bool $onlyContent = false): void
    {
        $files = Files::getFiles($path);
        if (!empty($files)) {
            Files::removeFile($files);
        }

        $directories = Files::getDirectories($path);
        if (!empty($directories)) {
            foreach ($directories as $directory) {
                Files::removeDirectory($directory);
            }
        }

        if (!$onlyContent) {
            rmdir($path);
        }
    }

    public static function isDirectoryEmpty(string $path): bool
    {
        $files = Files::getFiles($path);
        if (!empty($files)) {
            return false;
        }

        $directories = Files::getDirectories($path);
        if (!empty($directories)) {
            return false;
        }

        return true;
    }

    public static function getFiles(string $path, bool $recursively = false): array
    {
        $pathCopy = Files::sanitizePath($path);
        $result = [];
        $scanned = scandir($pathCopy);

        foreach ($scanned as $file) {
            $fullPath = $pathCopy . DIRECTORY_SEPARATOR . $file;
            if ($file != '.' && $file != '..' && is_file($fullPath)) {
                $result[] = $fullPath;
            }
        }

        if ($recursively) {
            foreach (Files::getDirectories($pathCopy) as $directory) {
                $files = Files::getFiles($directory, true);
                $result = array_merge($result, $files);
            }
        }

        return $result;
    }

    public static function getDirectories(string $path, bool $recursively = false): array
    {
        $pathCopy = Files::sanitizePath($path);
        $result = [];
        $scanned = scandir($pathCopy);

        if ($recursively) {
            foreach (Files::getDirectories($pathCopy) as $directory) {
                $result = array_merge($result, Files::getDirectories($directory, true));
            }
        }

        foreach ($scanned as $file) {
            $fullPath = $pathCopy . DIRECTORY_SEPARATOR . $file;
            if ($file != '.' && $file != '..' && is_dir($fullPath)) {
                $result[] = $fullPath;
            }
        }

        return $result;
    }

    public static function getFileContent(string $path): string
    {
        $path = Files::sanitizePath($path);
        return file_get_contents($path);
    }

    public static function putFileContent(string $path, string $content)
    {
        $path = Files::sanitizePath($path);

        if (!Files::exists($path)) {
            Files::createFile($path);
        }

        file_put_contents($path, $content);
    }

    private static function sanitizePath(string $path): string {
        return rtrim(trim($path), DIRECTORY_SEPARATOR);
    }
}