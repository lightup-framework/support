<?php

namespace Lightup\Support\Route;

class Route
{
    private array $routes;

    public function __construct()
    {
        $this->routes = [
            'get' => [],
            'post' => [],
            'put' => [],
            'patch' => [],
            'delete' => [],
        ];
    }

    public function get($route, $what, $middlewares = []) : void
    {
        $this->addRoute('get', $route, $what, $middlewares);
    }

    public function post($route, $what, $middlewares = []) : void
    {
        $this->addRoute('post', $route, $what, $middlewares);
    }

    public function put($route, $what, $middlewares = []) : void
    {
        $this->addRoute('put', $route, $what, $middlewares);
    }

    public function patch($route, $what, $middlewares = []) : void
    {
        $this->addRoute('patch', $route, $what, $middlewares);
    }

    public function delete($route, $what, $middlewares = []) : void
    {
        $this->addRoute('delete', $route, $what, $middlewares);
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    private function addRoute($method, $route, $what, $middlewares): void
    {
        $this->routes[$method][] = [
            'route' => trim($route, '\/'),
            'what' => $what,
            'middlewares' => $middlewares,
        ];
    }
}