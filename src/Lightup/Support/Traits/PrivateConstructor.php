<?php

namespace Lightup\Support\Traits;

trait PrivateConstructor
{
    private function __construct()
    {

    }
}