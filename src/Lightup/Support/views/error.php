<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Projekt | WDPAI</title>

        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;700&display=swap" rel="stylesheet">

        <style>
            body {
                margin: 0;
                padding: 0;
                font-family: 'Roboto', sans-serif;
                background-color: #fcfcfc;
                color: #393E41;
                min-width: 100vw;
                min-height: 100vh;
            }

            .center {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                font-weight: bold;
                font-size: 32px;
            }
        </style>
    </head>

    <body>
        <div class="center">
            <p>Error {{ $status_code }}</p>
        </div>
    </body>

</html>
