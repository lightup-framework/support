<?php

namespace Lightup\Support\Http\Controller;

use Lightup\Support\Facade\Response;

class ErrorController
{
    public function error($status_code)
    {
        return Response::view()
            ->setName('error')
            ->setStatusCode($status_code)
            ->setParameters([
                'status_code' => $status_code,
            ]);
    }
}