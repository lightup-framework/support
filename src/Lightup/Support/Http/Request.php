<?php

namespace Lightup\Support\Http;

use Lightup\Framework\Http\Request as FrameworkRequest;
use Lightup\Support\Router\Router;

class Request implements FrameworkRequest
{
    private array $inputs;
    private string $method;
    private string $path;

    public function __construct()
    {
        $this->inputs = $_REQUEST;
        $this->method = strtolower($this->getInput('_method', $_SERVER['REQUEST_METHOD']));
        $this->path = trim(explode('?', $_SERVER['REQUEST_URI'])[0], '\/');
    }

    public function getMethod() : string
    {
        return $this->method;
    }

    public function getPath() : string
    {
        return $this->path;
    }

    public function getInput($key, string $default = ""): string
    {
        if (array_key_exists($key, $this->inputs)) {
            return $this->inputs[$key];
        }

        return $default;
    }

    public function getInputs(): array
    {
        return $this->inputs;
    }

    public function setInput(string $key, string $value): void
    {
        $this->inputs[$key] = $value;
    }

    public function swap(array $inputs): void
    {
        $this->inputs = $inputs;
    }
}