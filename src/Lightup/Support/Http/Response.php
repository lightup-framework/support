<?php

namespace Lightup\Support\Http;

use Lightup\Support\Response\Redirect;
use Lightup\Framework\Http\Response as ResponseType;
use Lightup\Support\Response\View;

class Response
{
    private ResponseType $response;

    public function view(): View
    {
        $responseType = new View();
        $this->response = $responseType;

        return $responseType;
    }

    public function redirect(): Redirect
    {
        $responseType = new Redirect();
        $this->response = $responseType;

        return $responseType;
    }

    public function getResponse(): ResponseType
    {
        return $this->response;
    }
}