<?php

namespace Lightup\Support\Handler;

use Throwable;

interface ExceptionHandler
{
    public function handle(Throwable $throwable);
}