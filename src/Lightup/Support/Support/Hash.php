<?php

namespace Lightup\Support\Support;

use Lightup\Support\Traits\PrivateConstructor;

class Hash
{
    use PrivateConstructor;

    public static function make(string $str): string
    {
        $options = [
            'cost' => 12,
        ];

        return password_hash($str, PASSWORD_BCRYPT, $options);
    }

    public static function verify($str, $hash): bool
    {
        return password_verify($str, $hash);
    }
}