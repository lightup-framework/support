<?php

namespace Lightup\Support\Support;

use Lightup\Support\Traits\PrivateConstructor;

class Session
{
    use PrivateConstructor;

    public static function start(): void
    {
        session_start();
    }

    public static function check(string $key): bool
    {
        return array_key_exists($key, $_SESSION);
    }

    public static function set(string $key, mixed $value): void
    {
        $_SESSION[$key] = $value;
    }

    public static function get(string $key): mixed
    {
        return $_SESSION[$key];
    }

    public static function unset(string $key): void
    {
        unset($_SESSION[$key]);
    }

    public static function writeClose(): void
    {
        session_write_close();
    }

    public static function destroy(): void
    {
        session_unset();
        session_destroy();
    }

    public static function pull(string $key): mixed
    {
        $result = static::get($key);
        static::unset($key);

        return $result;
    }
}