<?php

namespace Lightup\Support\Support\Validator\Rules;

use Lightup\Framework\Validator\Rule;

class Required extends Rule
{
    public function message(string $attribute): string
    {
        return "Attribute {$attribute} is required";
    }

    public function passes(string $attribute, mixed $value): bool
    {
        return !empty($value);
    }
}