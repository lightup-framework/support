<?php

namespace Lightup\Support\Support\Validator\Rules;

use Lightup\Framework\Validator\Rule;
use Lightup\Support\Http\Request;

class Same extends Rule
{
    private Request $request;

    private string $otherAttribute;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function message(string $attribute): string
    {
        return "{$attribute} and {$this->otherAttribute} are different";
    }

    public function passes(string $attribute, mixed $value): bool
    {
        return $value == $this->request->getInput($this->otherAttribute);
    }

    public function additional(string $param)
    {
        $this->otherAttribute = $param;
    }
}