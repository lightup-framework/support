<?php

namespace Lightup\Support\Facade;

use Lightup\Support\Facade;
use Lightup\Support\Router\Router as RouterFacadeAccessor;

/**
 * Class Router
 * @package Framework\Facade
 * @method array route()
 */
class Router extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return RouterFacadeAccessor::class;
    }
}