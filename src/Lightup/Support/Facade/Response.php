<?php

namespace Lightup\Support\Facade;

use Lightup\Support\Facade;
use Lightup\Support\Http\Response as ResponseCore;
use Lightup\Support\Response\Redirect;
use Lightup\Support\Response\View;

/**
 * Class Response
 * @package Lightup\Support\Facade
 * @method static View view()
 * @method static Redirect redirect()
 */
class Response extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return ResponseCore::class;
    }
}