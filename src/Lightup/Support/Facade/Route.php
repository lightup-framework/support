<?php

namespace Lightup\Support\Facade;

use Lightup\Support\Facade;
use Lightup\Support\Route\Route as RouteFacadeAccessor;

/**
 * Class Route
 * @package Lightup\Support\Facade
 * @method static void get($route, $what, $middleware = [])
 * @method static void post($route, $what, $middleware = [])
 * @method static void put($route, $what, $middleware = [])
 * @method static void patch($route, $what, $middleware = [])
 * @method static void delete($route, $what, $middleware = [])
 * @method static array getRoutes()
 */
class Route extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return RouteFacadeAccessor::class;
    }
}