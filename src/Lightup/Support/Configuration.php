<?php

namespace Lightup\Support;

use Lightup\Framework\Configuration as ConfigurationContract;
use Lightup\Support\Registry\Registry;

class Configuration implements ConfigurationContract
{
    private Registry $cache;

    public function __construct()
    {
        $this->cache = new Registry();
    }

    public function get(string $key, mixed $default = null): mixed
    {
        $splitted = explode('.', $key);
        if ($this->cache->has($splitted[0])) {
            if (count($splitted) == 1) {
                return $this->cache->get($splitted[0]);
            }

            return $this->cache->get($splitted[0])[$splitted[1]];
        }

        $arr = require base_path(['config', $splitted[0] . '.php']);
        $this->cache->add($splitted[0], $arr);

        if (count($splitted) == 1) {
            return $arr;
        }

        return $arr[$splitted[1]];
    }
}